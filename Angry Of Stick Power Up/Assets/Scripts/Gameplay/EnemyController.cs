﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using MoreMountains.NiceVibrations;

public enum EEnemyType
{
    Normal,
    Boss
}

public class EnemyController : MonoBehaviour
{
    private Animator m_animator;
    private Rigidbody m_rigidbody;
    [SerializeField] private EEnemyType m_enemyType = EEnemyType.Normal;
    [SerializeField] private SkinnedMeshRenderer m_meshRenderer;

    public EEnemyType EnemyType { get => m_enemyType; set => m_enemyType = value; }

    private void Awake()
    {
        m_animator = GetComponentInChildren<Animator>();
        m_rigidbody = GetComponent<Rigidbody>();
    }

    private void OnDestroy()
    {
        DOTween.Kill(this);
    }

    public void Kill()
    {
        gameObject.layer = 6;
        m_animator.SetTrigger("die");
        GetComponent<BoxCollider>().isTrigger = false;
        m_rigidbody.useGravity = true;
        m_rigidbody.constraints = RigidbodyConstraints.FreezeRotation;

        const float delayTimeAnimPlayerAttack = 0.05f;
        DOVirtual.DelayedCall(delayTimeAnimPlayerAttack, () =>
        {
            m_rigidbody.AddForce(-(transform.forward - transform.up) * 400);
            m_meshRenderer.material.color = Color.gray;

            MMVibrationManager.StopAllHaptics(true);
            MMVibrationManager.Haptic(HapticTypes.SoftImpact);
        });

        const float delayTimeAnimDrop = 2;
        DOVirtual.DelayedCall(delayTimeAnimDrop, () =>
        {
            GetComponent<BoxCollider>().isTrigger = true;
            Destroy(gameObject, 0.5f);
        }).SetId(this);
    }

    public void PlayTriggerPower()
    {
        m_animator.SetBool("run", false);
        m_animator.SetTrigger("power_up");
    }

    public void PlayTriggerPunch()
    {
        m_animator.SetTrigger("punch");
    }

    public void PlayTriggerHit()
    {
        m_animator.SetTrigger("hit");
    }
}