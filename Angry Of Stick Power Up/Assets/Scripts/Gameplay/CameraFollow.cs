﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Transform m_posFilm;
    [SerializeField] private Transform m_target;

    [SerializeField] private Vector3 m_offset;
    [SerializeField] private float m_lerpSpeed;
    private Quaternion m_rotateOrigin;
    private Vector3 m_posOrigin;
    private Vector3 m_posOriginFiml;
    private bool m_allowFollow = false;
    public Transform Target { get => m_target; set => m_target = value; }

    private void Awake()
    {
        m_rotateOrigin = transform.localRotation;
        m_posOrigin = transform.position;
    }

    private void Start()
    {
        StartCoroutine(Initing());
    }

    private void LateUpdate()
    {
        if (m_allowFollow == false)
        {
            return;
        }
        Vector3 _newTarget = m_target.position + m_offset;
        Vector3 _newPos = new Vector3(transform.position.x, transform.position.y, _newTarget.z);
        transform.position = _newPos;/*Vector3.Lerp(transform.position, _newPos, m_lerpSpeed);*/
    }

    public void Reset()
    {
        m_allowFollow = true;
        transform.position = m_posOrigin;
        transform.DORotateQuaternion(m_rotateOrigin, 0).SetId(this);
        Target = GameManager.Instance.CurrentLevel.Player.transform;
    }

    public void Stop()
    {
        m_allowFollow = false;
    }

    private IEnumerator Initing()
    {
        yield return new WaitUntil(() => m_target != null);
        m_offset = transform.position - m_target.position;
        m_allowFollow = true;
    }

    public void MoveFilm()
    {
        m_posOriginFiml = transform.position;
        const float DURATION = 2.0f;
        transform.DORotateQuaternion(m_posFilm.rotation, DURATION);
        transform.DOMove(m_posFilm.position, DURATION);
    }

    public void MoveFilmComback()
    {
        const float DURATION = 1.0f;
        transform.DORotateQuaternion(m_rotateOrigin, DURATION);
        transform.DOMove(m_posOriginFiml, DURATION);
    }
}