using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockController : MonoBehaviour
{
    [SerializeField] private Transform m_obstacles;
    [SerializeField] private Transform m_plans;
    private bool m_isShowed = false;

    public void Init()
    {
        m_obstacles.gameObject.SetActive(false);
        m_plans.gameObject.SetActive(true);
        for (int i = 1; i < m_plans.childCount; i++)
        {
            m_plans.GetChild(i).gameObject.SetActive(false);
        }
    }

    public void Show()
    {
        if (m_isShowed)
        {
            return;
        }
        m_isShowed = true;
        gameObject.SetActive(true);
        m_obstacles.gameObject.SetActive(true);

        foreach (Transform item in m_plans)
        {
            item.gameObject.SetActive(true);
        }
    }
}