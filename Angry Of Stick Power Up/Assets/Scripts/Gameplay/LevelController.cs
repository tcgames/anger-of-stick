using DG.Tweening;
using GameAnalyticsSDK;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    [SerializeField] private int m_coinWin = 100;
    [SerializeField] private int m_coinLose = 20;
    [SerializeField] private int m_coinAdsReward = 300;
    [SerializeField] private PlayerController m_player;
    [SerializeField] private StageLevel m_stageLevel;
    [SerializeField] private BlockController[] m_blocks;
    private PlayerController m_playerFilm;
    private EnemyController m_bossFilm;
    private bool m_isInBattle = false;

    public StageLevel StageLevel { get => m_stageLevel; set => m_stageLevel = value; }

    public PlayerController Player { get => m_player; set => m_player = value; }
    public int CoinWin { get => m_coinWin; set => m_coinWin = value; }
    public int CoinLose { get => m_coinLose; set => m_coinLose = value; }
    public int CoinAdsReward { get => m_coinAdsReward; set => m_coinAdsReward = value; }
    public bool IsInBattle { get => m_isInBattle; }

    public void InitLevel()
    {
        StageLevel = StageLevel.Pre;
        GameManager.Instance.Camera.GetComponent<CameraFollow>().Target = m_player.transform;
        m_player.Init(this);
        foreach (BlockController item in m_blocks)
        {
            item.Init();
        }
    }

    public void StartLevel()
    {
        StageLevel = StageLevel.Playing;
        UIManager.Instance.UIMainMenu.HideTutorial();
    }

    public void LevelWin()
    {
        const float TIME_DELAY = 0.5f;
        const float TIME_WAIT_FX = 3;
        const float POS_Y = 2.5f;
        const float ROTATE_X = 17;
        const float ROTATE_Y = 0;
        const float ROTATE_Z = 0;
        const float DURATION_ROTATE = 0.5f;
        const float DURATION_MOVE_Y = 0.5f;

        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "Level " + (GameManager.Instance.CurrentIndexLevel + 1));

        DOVirtual.DelayedCall(TIME_DELAY, () =>
        {
            Camera.main.transform.DOMoveY(POS_Y, DURATION_MOVE_Y);
            Camera.main.transform.DORotate(new Vector3(ROTATE_X, ROTATE_Y, ROTATE_Z), DURATION_ROTATE).OnComplete(() =>
            {
                GameManager.Instance.FXWin.ActiveFX();
                DOVirtual.DelayedCall(TIME_WAIT_FX, () =>
                {
                    UIManager.Instance.UIWinController.ShowWin();
                });
            });
        });
    }

    public void LevelLose()
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, "Level " + (GameManager.Instance.CurrentIndexLevel + 1));

        UIManager.Instance.UILoseController.ShowLose();
    }

    public void RewardCoinWinPlayer()
    {
        UserProfileManager.Instance.UserProfile.Coin += CoinWin;
        UIManager.Instance.UIMainMenu.UpdateCoinUI();
    }

    public void RewardCoinLosePlayer()
    {
        UserProfileManager.Instance.UserProfile.Coin += CoinLose;
        UIManager.Instance.UIMainMenu.UpdateCoinUI();
    }

    public void RewardCoinWatchAdsPlayer()
    {
        UserProfileManager.Instance.UserProfile.Coin += CoinAdsReward;
        UIManager.Instance.UIMainMenu.UpdateCoinUI();
    }

    public void ActiveBattle(PlayerController player, EnemyController boss)
    {
        m_isInBattle = true;
        player.PlayTriggerPower();
        boss.PlayTriggerPower();
        Camera.main.GetComponent<CameraFollow>().Stop();
        Camera.main.GetComponent<CameraFollow>().MoveFilm();

        m_playerFilm = player;
        m_bossFilm = boss;

        const float WATING_CAM = 2;
        StartCoroutine(StartBattle(WATING_CAM, () =>
        {
            Camera.main.GetComponent<CameraFollow>().MoveFilmComback();
        }));
    }

    private void PlayerPunchEnemy(PlayerController player, EnemyController boss)
    {
        DOTween.Kill(this);
        player.PlayTriggerPunch();

        const float WATING_PUNCH = 0.1f;
        DOVirtual.DelayedCall(WATING_PUNCH, () =>
        {
            boss.PlayTriggerHit();
        }).SetId(this);
        DOVirtual.DelayedCall(WATING_PUNCH + .5f, () =>
        {
            boss.PlayTriggerHit();
        }).OnComplete(() =>
        {
        }).SetId(this);
    }

    private void EnemPunchyPlayer(PlayerController player, EnemyController boss)
    {
        DOTween.Kill(this);
        boss.PlayTriggerPunch();

        const float WATING_PUNCH = 0.1f;
        DOVirtual.DelayedCall(WATING_PUNCH, () =>
        {
            player.PlayTriggerHit();
        }).SetId(this);
        DOVirtual.DelayedCall(WATING_PUNCH + .5f, () =>
        {
            player.PlayTriggerHit();
        }).OnComplete(() =>
        {
        }).SetId(this);
    }

    private bool IsMouseDown()
    {
        return Input.GetMouseButtonDown(0);
    }

    private IEnumerator StartBattle(float WATING_CAM, Action onComplete)
    {
        yield return new WaitForSeconds(WATING_CAM);

        const float WATING_PUNCH = 1f;
        const int numberPunch = 2;
        for (int i = 0; i < numberPunch; i++)
        {
            PlayerPunchEnemy(m_playerFilm, m_bossFilm);
            yield return new WaitForSeconds(WATING_PUNCH);
            EnemPunchyPlayer(m_playerFilm, m_bossFilm);
            yield return new WaitForSeconds(WATING_PUNCH);
        }

        m_playerFilm.PlayRun();
        m_playerFilm.transform.DOMoveZ(m_playerFilm.transform.position.z + 1.0f, 1).OnComplete(() =>
        {
            m_playerFilm.PlayTriggerAttck_2();
            DOVirtual.DelayedCall(1.0f, () =>
            {
                m_bossFilm.Kill();
                onComplete?.Invoke();
            }).SetId(this);
        });
    }
}

public enum StageLevel
{
    Pre,
    Playing,
    End
}