using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyController : MonoBehaviour
{
    [SerializeField] private DoorController m_doorController;

    public void OpenDoor()
    {
        m_doorController.OpenDoor();
        GetComponent<MeshRenderer>().material.color = Color.white;
    }
}