using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanController : MonoBehaviour
{
    private BlockController m_blockController;

    private void Awake()
    {
        m_blockController = GetComponentInParent<BlockController>();
    }

    public void Show()
    {
        m_blockController.Show();
    }
}