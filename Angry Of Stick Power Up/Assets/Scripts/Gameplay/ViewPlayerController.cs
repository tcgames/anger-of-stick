using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewPlayerController : MonoBehaviour
{
    private void Awake()
    {
        gameObject.SetActive(true);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Plan"))
        {
            other.transform.GetComponent<PlanController>().Show();
        }
    }
}