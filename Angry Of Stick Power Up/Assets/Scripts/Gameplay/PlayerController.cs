using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class PlayerController : MonoBehaviour
{
    private const string TAG_ENEMY = "Enemy";
    private const string POINT_END = "PointEnd";
    private const string OBSTACLE = "Obstacle";
    private const string HOLE = "Hole";
    private const string KEY = "Key";

    [SerializeField] private Transform m_model;
    [SerializeField] private float m_moveSpeed;
    [SerializeField] private float m_speedControl;
    [SerializeField] private float m_clampX;
    [SerializeField] private LevelController m_level;

    private Vector3 m_dir;
    private Vector3 m_delta;
    private Vector3 m_posTo;
    private Vector3 m_posOld;
    private Animator m_animator;
    private Rigidbody m_rigidbody;
    private bool m_live = true;
    private bool m_firstTap = true;

    private void Awake()
    {
        m_animator = GetComponentInChildren<Animator>();
        m_rigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (m_level.StageLevel != StageLevel.Playing || m_live == false || m_level.IsInBattle)
        {
            if (Input.GetMouseButtonDown(0) && m_firstTap)
            {
                m_firstTap = false;
                m_level.StartLevel();
            }
            else
            {
                return;
            }
        }

        m_animator.SetBool("run", true);

        Control();
        MoveForward();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (m_level.StageLevel != StageLevel.Playing)
        {
            return;
        }

        if (other.tag.Equals(TAG_ENEMY))
        {
            AttackEnemy(other);
            return;
        }

        if (other.tag.Equals(POINT_END))
        {
            DOVirtual.DelayedCall(2.0f, () =>
            {
                TriggerPointEnd(other);
            }).SetId(this);
            return;
        }

        if (other.tag.Equals(OBSTACLE))
        {
            m_live = false;
            Camera.main.transform.GetComponent<CameraFollow>().Stop();

            PlayerDie(() =>
            {
                // HOLE
                if (other.name.Equals(HOLE))
                {
                    TriggerHole(other);
                    return;
                }

                // DOOR
                TriggerDoorWall(other);
                return;
            });
        }

        if (other.tag.Equals(KEY))
        {
            TriggerKey(other);
        }
    }

    private void OnDestroy()
    {
        DOTween.Kill(this);
    }

    public void Init(LevelController level)
    {
        m_level = level;
    }

    public void Control()
    {
        if (Input.GetMouseButtonDown(0))
        {
            m_posOld = Input.mousePosition;
        }
        else if (Input.GetMouseButton(0))
        {
            m_delta = Input.mousePosition - m_posOld;
            m_posOld = Input.mousePosition;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            m_delta = Vector3.zero;
            m_dir = Vector3.zero;
        }

        if (m_delta != Vector3.zero)
        {
            m_dir.x = -m_delta.x * Time.smoothDeltaTime * m_speedControl;
            m_dir.y = 0;
            m_posTo = m_model.transform.position - m_dir;
            m_posTo.x = Mathf.Clamp(m_posTo.x, -m_clampX, m_clampX);
            m_model.transform.position = m_posTo;
        }
    }

    private void PlayerDie(Action onComplete)
    {
        m_live = false;
        onComplete?.Invoke();
    }

    private void AttackEnemy(Collider other)
    {
        EnemyController enemy = other.GetComponent<EnemyController>();
        if (enemy.EnemyType == EEnemyType.Normal)
        {
            m_animator.SetTrigger("attack");
            other.GetComponent<EnemyController>().Kill();
        }
        else
        {
            m_level.ActiveBattle(this, enemy);
        }
    }

    private void TriggerHole(Collider other)
    {
        Vector3 pos = other.transform.position;
        transform.DOMove(pos, 0.5f).SetId(this);
        transform.DOScale(Vector3.zero, 0.65f).SetId(this).OnComplete(() =>
        {
            m_level.LevelLose();
        });
    }

    private void TriggerDoorWall(Collider other)
    {
        m_animator.SetBool("run", false);
        m_animator.SetTrigger("die");
        DOVirtual.DelayedCall(0.5f, () =>
        {
            m_level.LevelLose();
        });
    }

    private void TriggerKey(Collider other)
    {
        other.transform.GetComponent<KeyController>().OpenDoor();
    }

    private void TriggerPointEnd(Collider other)
    {
        m_level.StageLevel = StageLevel.End;
        m_animator.SetBool("run", false);
        m_animator.SetTrigger("idle");
        transform.DOLocalRotate(new Vector3(0, 180, 0), 0.5f).OnComplete(() =>
        {
            m_level.LevelWin();
        }).SetId(this);
    }

    private void MoveForward()
    {
        transform.Translate(transform.forward * m_moveSpeed * Time.smoothDeltaTime);
    }

    public void PlayTriggerPower()
    {
        m_animator.SetBool("run", false);
        m_animator.SetTrigger("power_up");
    }

    public void PlayTriggerPunch()
    {
        m_animator.SetTrigger("punch");
    }

    public void PlayTriggerHit()
    {
        m_animator.SetTrigger("hit");
    }

    public void PlayTriggerAttck_2()
    {
        m_animator.SetBool("run", false);
        m_animator.SetTrigger("attack_2");
    }

    public void PlayRun()
    {
        m_animator.SetBool("run", true);
    }
}