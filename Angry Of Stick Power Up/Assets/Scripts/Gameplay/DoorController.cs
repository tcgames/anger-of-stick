using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DoorController : MonoBehaviour
{
    [SerializeField] private float m_leftTarget = -2.5f;
    [SerializeField] private float m_rightTarget = 2.5f;
    [SerializeField] private Transform m_left;
    [SerializeField] private Transform m_right;

    private void Awake()
    {
        m_left.GetComponent<MeshRenderer>().material.color = Color.red;
        m_right.GetComponent<MeshRenderer>().material.color = Color.red;
    }

    public void OpenDoor()
    {
        m_left.DOLocalMoveX(m_leftTarget, 0.2f);
        m_right.DOLocalMoveX(m_rightTarget, 0.2f);

        m_left.GetComponent<MeshRenderer>().material.color = Color.white;
        m_right.GetComponent<MeshRenderer>().material.color = Color.white;
    }
}