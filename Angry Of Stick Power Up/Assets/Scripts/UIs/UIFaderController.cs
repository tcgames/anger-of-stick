﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class UIFaderController : MonoBehaviour
{
    Image img;
    private void Awake()
    {
        img = GetComponent<Image>();
    }
    public void FadeShow(Action onComplete)
    {
        var tempColor = img.color;

        DOVirtual.Float(1.0f, 0.0f, 1.0f, (x) =>
        {
            tempColor.a = x;
            img.color = tempColor;
        }).OnComplete(() =>
        {
            onComplete?.Invoke();
        });
    }
    public void FadeHide(Action onComplete)
    {
        var tempColor = img.color;
        DOVirtual.Float(0.0f, 1.0f, 1.0f, (x) =>
        {
            tempColor.a = x;
            img.color = tempColor;
        }).OnComplete(() =>
        {
            onComplete?.Invoke();
        });
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }
    public void Show()
    {
        gameObject.SetActive(true);
    }
}
