using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIWinController : MonoBehaviour
{
    [SerializeField] private Button m_btnPlay;
    [SerializeField] private TMPro.TextMeshProUGUI m_tmpCoin;

    private void Awake()
    {
        m_btnPlay.onClick.AddListener(() =>
        {
            GameManager.Instance.CurrentLevel.RewardCoinWinPlayer();
            GameManager.Instance.LoadNextLevelIndex();
            Hide();
        });
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void UpdateUICoin()
    {
        m_tmpCoin.text = "+" + GameManager.Instance.CurrentLevel.CoinWin.ToString();
    }

    public void ShowWin()
    {
        Show();
        UpdateUICoin();
    }
}