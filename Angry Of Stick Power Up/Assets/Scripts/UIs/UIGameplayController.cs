using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIGameplayController : MonoBehaviour
{
    [SerializeField] private VariableJoystick m_joystick;

    public VariableJoystick Joystick { get => m_joystick; set => m_joystick = value; }
}
