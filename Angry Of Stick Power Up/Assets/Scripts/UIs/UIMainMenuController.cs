using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIMainMenuController : MonoBehaviour
{
    [SerializeField] private Button m_btnPlay;
    [SerializeField] private Transform m_tutorial;
    [SerializeField] private TextMeshProUGUI m_tmpCoin;

    private void Awake()
    {
        m_btnPlay.onClick.AddListener(() =>
        {
            GameManager.Instance.CurrentLevel.StartLevel();
            HideBtnPlay();
            HideTutorial();
        });
    }

    public void HideTutorial()
    {
        m_tutorial.gameObject.SetActive(false);
    }

    public void Show()
    {
        gameObject.SetActive(true);
        ShowTutorial();
    }

    public void ShowTutorial()
    {
        m_tutorial.gameObject.SetActive(true);
    }

    public void UpdateCoinUI()
    {
        m_tmpCoin.text = UserProfileManager.Instance.UserProfile.Coin.ToString();
    }

    public void ShowBtnPlay()
    {
        m_btnPlay.gameObject.SetActive(true);
    }

    private void HideBtnPlay()
    {
        m_btnPlay.gameObject.SetActive(false);
    }
}