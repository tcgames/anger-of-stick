using DG.Tweening;
using GameAnalyticsSDK;
using GameAnalyticsSDK.Events;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    [SerializeField] private Transform m_camera;
    [SerializeField] private FXWinController m_fXWinController;
    [SerializeField] private int m_CurrentIndexLevel;
    [SerializeField] private LevelController[] m_Levels;
    [SerializeField] private LevelController m_CurrentLevel;
    [SerializeField] private LevelController m_OldLevel;

    public int CurrentIndexLevel { get => m_CurrentIndexLevel; }
    public LevelController CurrentLevel { get => m_CurrentLevel; set => m_CurrentLevel = value; }
    public Transform Camera { get => m_camera; set => m_camera = value; }
    public FXWinController FXWin { get => m_fXWinController; set => m_fXWinController = value; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        Input.multiTouchEnabled = false;
        GameAnalytics.Initialize();
        UserProfileManager.Instance.Init(() =>
        {
#if !UNITY_EDITOR
            m_CurrentIndexLevel = UserProfileManager.Instance.UserProfile.IndexLevel;
#endif
            LoadLevelIndex(m_CurrentIndexLevel);

            UIManager.Instance.UIMainMenu.UpdateCoinUI();
        });
    }

    public void LoadLevelIndex(int indexLevel)
    {
        if (m_CurrentLevel != null)
        {
            DestroyImmediate(m_CurrentLevel.gameObject);
        }
        m_CurrentLevel = Instantiate(m_Levels[indexLevel], transform);
        m_CurrentLevel.gameObject.SetActive(true);

        m_CurrentLevel.InitLevel();
        Camera.GetComponent<CameraFollow>().Target = CurrentLevel.Player.transform;

        m_OldLevel = Instantiate(m_CurrentLevel, transform);
        m_OldLevel.gameObject.SetActive(false);
    }

    public void LoadNextLevelIndex()
    {
        UIManager.Instance.UIFader.Show();
        UIManager.Instance.UIFader.FadeHide(() =>
        {
            if (m_CurrentLevel != null)
            {
                DestroyImmediate(m_CurrentLevel.gameObject);
            }

            m_CurrentIndexLevel++;

            if (m_CurrentIndexLevel >= m_Levels.Length)
            {
                m_CurrentIndexLevel = 0;
            }
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "Level " + (m_CurrentIndexLevel + 1));

            UserProfileManager.Instance.UserProfile.IndexLevel = m_CurrentIndexLevel;

            m_CurrentLevel = Instantiate(m_Levels[m_CurrentIndexLevel], transform);
            m_CurrentLevel.gameObject.SetActive(true);

            UIManager.Instance.UIMainMenu.Show();

            m_CurrentLevel.InitLevel();
            Camera.GetComponent<CameraFollow>().Reset();

            m_OldLevel = Instantiate(m_CurrentLevel, transform);
            m_OldLevel.gameObject.SetActive(false);

            DOVirtual.DelayedCall(0.5f, () =>
            {
                UIManager.Instance.UIFader.FadeShow(() =>
                {
                    UIManager.Instance.UIFader.Hide();
                });
            });
        });
    }

    public void LoadReplayLevel()
    {
        UIManager.Instance.UIFader.Show();
        UIManager.Instance.UIFader.FadeHide(() =>
        {
            if (m_CurrentLevel != null)
            {
                Destroy(m_CurrentLevel.gameObject);
            }

            m_CurrentLevel = Instantiate(m_OldLevel, transform);
            m_CurrentLevel.gameObject.SetActive(true);

            Camera.GetComponent<CameraFollow>().Reset();
            DOVirtual.DelayedCall(0.5f, () =>
            {
                UIManager.Instance.UIFader.FadeShow(() =>
                {
                    UIManager.Instance.UIFader.Hide();
                });
            });
        });
    }
}