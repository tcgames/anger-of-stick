using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FXWinController : MonoBehaviour
{
    [SerializeField] private ParticleSystem[] m_fxs;

    public void ActiveFX()
    {
        Debug.Log("ActiveFX");
        foreach (var item in m_fxs)
        {
            item.Play();
        }

        DOVirtual.DelayedCall(2.0f, () =>
        {
            foreach (var item in m_fxs)
            {
                item.Stop();
            }
        });
    }
}