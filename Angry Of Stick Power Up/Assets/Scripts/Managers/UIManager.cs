using UnityEngine;

public class UIManager : SingletonMonoAwake<UIManager>
{
    [SerializeField] private UIMainMenuController m_uIMainMenuController;
    [SerializeField] private UIGameplayController m_uIGameplayController;
    [SerializeField] private UIWinController m_uIWinController;
    [SerializeField] private UILoseController m_uILoseController;
    [SerializeField] private UIFaderController m_uIFaderController;
    public UIGameplayController UIGameplay { get => m_uIGameplayController; set => m_uIGameplayController = value; }
    public UIFaderController UIFader { get => m_uIFaderController; set => m_uIFaderController = value; }
    public UIMainMenuController UIMainMenu { get => m_uIMainMenuController; set => m_uIMainMenuController = value; }
    public UIWinController UIWinController { get => m_uIWinController; set => m_uIWinController = value; }
    public UILoseController UILoseController { get => m_uILoseController; set => m_uILoseController = value; }
}