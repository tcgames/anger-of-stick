﻿using System;
using System.IO;
using UnityEngine;

public class UserProfileManager : SingletonMonoAwake<UserProfileManager>
{
    [SerializeField] private UserProfile m_userProfile;
    private string m_tempFilePath;
    private bool m_isNewer = false;

    public UserProfile UserProfile { get => m_userProfile; set => m_userProfile = value; }
    public bool IsNewer { get => m_isNewer; set => m_isNewer = value; }

    public void Init(Action OnComplete)
    {
        m_tempFilePath = Application.persistentDataPath + "/UserProfile.data";

#if UNITY_EDITOR
        Debug.Log("TempFilePath ==> " + m_tempFilePath);
#endif
        if (!File.Exists(m_tempFilePath))//make a file if not exists
        {
            m_isNewer = true;
            using (StreamWriter newTask = new StreamWriter(m_tempFilePath, false))
            {
                m_userProfile = new UserProfile();
                newTask.WriteLine(JsonUtility.ToJson(m_userProfile));
                newTask.Close();
            }
        }
        else// read and update file
        {
            StreamReader reader = new StreamReader(m_tempFilePath);
            JsonUtility.FromJsonOverwrite(reader.ReadLine(), m_userProfile);
            reader.Close();
        }

        OnComplete?.Invoke();
    }

    private void SaveData()
    {
        if (m_tempFilePath != null)
        {
            using (StreamWriter newTask = new StreamWriter(m_tempFilePath, false))
            {
                string str = JsonUtility.ToJson(m_userProfile);
                newTask.WriteLine(str);
                newTask.Close();
            }
        }
    }

    private void OnApplicationPause(bool pause)
    {
        SaveData();
    }

    private void OnApplicationQuit()
    {
        SaveData();
    }
}

[System.Serializable]
public class UserProfile
{
    public int Coin;
    public int IndexLevel;

    public UserProfile()
    {
        Coin = 0;
        IndexLevel = 0;
    }
}